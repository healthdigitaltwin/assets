# HealthDigitalTwin

[![License](https://img.shields.io/badge/license-Apache%202.0-blue.svg)](LICENSE)

HealthDigitalTwin is a healthcare project that enables providers to send data in HL7 v2.7 or FHIR v4 format for processing and analysis. It incorporates a digital twin concept to represent and analyze patient health data, allowing for advanced analytics, AI modeling, and data-driven decision-making.

## Features

- Supports data ingestion in HL7 v2.7 format over MLLP+TLS or FHIR v4 format over REST+SSL.
- Validates incoming HL7 and FHIR data for compliance and correctness.
- Converts HL7 v2.7 data to FHIR v4 format for unified processing.
- Extracts unstructured text from ORU messages and documents from MDM_T02 messages.
- Utilizes Natural Language Processing (NLP) techniques such as Named Entity Recognition (NER), Relation Extraction (RE), and Assertion to process extracted text.
- Generates FHIR resources based on the extracted data.
- Provides a queue for data storage and retrieval.
- Enables data scientists to query the queue and build datasets for training and testing multimodal AI models in JupyterLab notebooks.
- Supports deployment of JupyterLab notebooks as microservices that subscribe to the queue for real-time data processing.

## Use Case: Radiology Request Assessment

The use of AI in radiology request assessment is justified by the significant proportion of unnecessary examinations. This project aims to automate the vetting process by extracting and analyzing clinical information from request justifications using LLMs and AutoAI. The AI system determines the appropriateness of requests and generates comprehensive response explanations using LLMs. By automating this process, valuable workforce time can be saved, optimizing resource utilization and improving patient care.

[Link to Radiology Vetting Documentation](/Use%20Cases/Radiology%20Vetting/radiology_vetting.md)

## Getting Started

To get started with HealthDigitalTwin, follow these steps:

1. [Installation instructions - Add steps to install dependencies and set up the project]
2. [Configuration instructions - Provide guidance on configuring the system]
3. [Usage instructions - Explain how to use the system and provide examples]

## Contributing

We welcome contributions to HealthDigitalTwin! To contribute, please follow the guidelines outlined in [CONTRIBUTING.md](CONTRIBUTING.md).

## License

HealthDigitalTwin is licensed under the [Apache License 2.0](LICENSE).

## Contact

If you have any questions or suggestions regarding HealthDigitalTwin, please reach out to [maintainer's name] at [maintainer's email address].

TO DO