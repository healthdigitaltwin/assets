# Project Dependencies

This table lists the dependencies used in the project, along with their respective versions.

| Dependency                                             | New Version  | Old Version  |
|--------------------------------------------------------|--------------|--------------|
| org.apache.commons:commons-math3                        | 3.6.1        | 3.6.1        |
| com.google.guava:guava                                  | 31.0.1-jre  | 30.0         |
| ca.uhn.hapi.fhir:hapi-fhir-structures-r4                | 6.6.1        | 6.4.0        |
| ca.uhn.hapi.fhir:hapi-fhir-structures-r5                | 6.6.1        | 6.4.0        |
| ca.uhn.hapi.fhir:hapi-fhir-validation                   | 6.6.1        | 6.4.0        |
| ca.uhn.hapi.fhir:hapi-fhir-validation-resources-r4      | 6.6.1        | 6.4.0        |
| com.fasterxml.jackson.core:jackson-databind             | 2.15.2       | 2.13.4.2     |
| com.fasterxml.jackson.datatype:jackson-datatype-jsr310  | 2.15.2       | 2.10.1       |
| com.fasterxml.jackson.datatype:jackson-datatype-jdk8    | 2.15.2       | 2.10.1       |
| com.fasterxml.jackson.dataformat:jackson-dataformat-yaml | 2.15.2       | 2.14.0       |
| org.apache.commons:commons-text                         | 1.10.0       | 1.10.0       |
| org.apache.commons:commons-jexl3                        | 3.3          | 3.1          |
| com.jayway.jsonpath:json-path                           | 2.8.0        | 2.6.0        |
| org.apache.commons:commons-configuration2               | 2.9.0        | 2.8.0        |
| org.apache.commons:commons-collections4                 | 4.4          | 4.4          |
| org.apache.commons:commons-compress                     | 1.23.0       | 1.21         |
| com.ibm.fhir:fhir-registry                              | 4.11.1       | 4.10.2       |
| com.ibm.fhir:fhir-term                                  | 4.11.1       | 4.10.2       |
| commons-beanutils:commons-beanutils                     | 1.9.4        | 1.9.4        |
| org.springframework.boot:spring-boot-starter-web        | 2.7.13       | N/A          |
| org.springframework.boot:spring-boot-starter-security   | 2.7.13       | N/A          |
| org.springframework.boot:spring-boot-starter-validation | 2.7.13       | N/A          |
| org.springframework.boot:spring-boot-starter-tomcat     | 2.7.13       | N/A          |
| org.apache.tomcat.embed:tomcat-embed-jasper             | 11.0.0-M7    | N/A          |
| org.apache.httpcomponents:httpclient                    | 4.5.14       | N/A          |
| ch.qos.logback:logback-classic                          | 1.4.8        | 1.4.0        |
| org.assertj:assertj-core                                | 3.24.2       | 3.9.0        |
| org.junit.jupiter:junit-jupiter-api                     | 5.7.2        | 5.8.2        |
| org.junit.jupiter:junit-jupiter-params                  | 5.7.2        | 5.8.2        |
| org.junit.jupiter:junit-jupiter-engine                  | 5.7.2        | 5.8.2        |
