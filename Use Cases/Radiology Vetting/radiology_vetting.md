# Use Case: Radiology Request Assessment

## Introduction
Radiology departments face the challenge of efficiently vetting and managing the large volume of referrals they receive. To address this issue and improve the quality of referrals while reducing the burden on radiologists, an AI-based project is proposed. The project aims to leverage advanced technologies, including Language Models (LLMs) and AutoAI, to automate the assessment of radiology requests. This comprehensive solution will involve extracting and analyzing clinical information from request justifications against referral policy guidelines, making informed determinations, and generating response explanations using LLMs.

## Justification for the Project
The justification for this AI project is based on the findings of the Léger survey conducted in 2019. The survey estimated that approximately 25% of radiology examinations received by radiologists are unnecessary. This high proportion of unnecessary examinations highlights the need for an improved vetting process to ensure that requests adhere to guidelines and are clinically justified. By implementing an AI-driven solution, the project aims to reduce the occurrence of unwarranted examinations, optimizing resource utilization and improving the patient journey.

## Understanding Request Justification using LLMs
To ensure accurate assessment of radiology requests, LLMs (Language Models) are utilized to analyze and understand the content of request justifications. Advanced Natural Language Processing (NLP) techniques are applied to extract important details, such as procedure information, patient demographics, and medical history. Named Entity Recognition (NER) algorithms play a crucial role in identifying and categorizing relevant entities like procedure codes (e.g., SNOMED-CT or NICIP) and counter-indications (e.g., hip replacement, diabetes, recent diagnostic investigations). By analyzing the extracted data, the AI model can gain insights into the clinical context and assess the appropriateness of the requested procedure.

When submitting a radiology request, it is important that the requester includes the following information:

- Clinical background: A comprehensive clinical background that outlines the patient's medical condition, symptoms, and relevant history.
- Question to be answered: The specific qoal, question or concern that the radiology examination aims to address.
- Patient details: The patient's full name, age, address, and contact number to accurately identify the individual.
- Patient location: The ward or location where the patient is currently located or receiving treatment.
- Requesting practitioner and consultant/GP: The names of both the requesting practitioner who initiated the referral and the consultant or GP responsible for the patient's care.

It's important to acknowledge that not all requests may include all the necessary information, and in the context of electronic order communications the information may be dispersed across structured and unstructured formats. In such cases, relying solely on LLMs may not be sufficient to address this challenge comprehensively.

To address the challenge of incomplete information in radiology requests and different data types, it is essential to adopt a comprehensive approach that combines the power of LLMs with other advanced techniques, including automated machine learning (AutoAI) classification models

LLM Prompt

```
Please perform Named Entity Recognition (NER) on the following radiology request and extract the following entities: Requested Procedure, Reason For Request (including Symptoms), Observations, Conditions, Previous Procedures, and Comorbidities.

Output Schema for CSV file:
[RequestedProcedure|[Value],PresentingSymptoms|[Value1]&[Value2]&...,RecentObservations|[ObservationKey1]^[ObservationValue1]&[ObservationKey2]^[ObservationValue2]&...,SuspectedCondition|[Value1]&[Value2]&...,PreviousProcedures|[Value1]&[Value2]&...,Comorbidities|[Value1]&[Value2]...]

Example Radiology Request: ["Patient presents with severe headache, dizziness, and blurred vision. Suspected intracranial pathology. MRI brain requested. Patient has a history of hypertension and diabetes and a lobectomy in 2008. Current observations: BP 150/90, HR 80, RR 18, Temp 37.5°C."]

Example Output: [RequestedProcedure|MRI brain,PresentingSymptoms|severe headache&dizziness&blurred vision,SuspectedCondition|intracranial pathology,RecentObservations|BP^150/90&HR^80&RR^18&Temp^37.5°C,PreviousProcedures|lobectomy in 2008,Comorbidities|hypertension&diabetes]

Radiology Request: ["<$RadiologyRequest>"]

```


Please make sure to clearly indicate the relevant clinical details in the request text and specify the assertion (present/hypothetical/history) for each extracted entity. For example, if the patient currently has a symptom, mark it as present. If it's a historical condition, mark it as history. If it's a possible condition, mark it as hypothetical.

Make sure to follow the format and structure of the provided JSON output. The model will process the request and populate the extracted information accordingly in the JSON structure, including the correct assertion status.


Example request (relevant clinical information)


```
Laparotomy 3/7 ago for bowel cancer
Increasing SOB and cough
RR 20, HR 100, Sats 90%
ABG normal
Wells Score 6
?PE ?Pneumonia
CTPA please
```

Example LLM output


```
[RequestedProcedure|CTPA,PresentingSymptoms|Increasing SOB&cough,SuspectedCondition|PE&Pneumonia,RecentObservations|RR^20&HR^100&Sats^90%,PreviousProcedures|Laparotomy for bowel cancer,Comorbidities|N/A]

```

## AutoAI for Determination and Decision-Making
The processed request justifications, along with additional contextual data, are then fed into an AutoAI system. This system leverages machine learning algorithms to make predictions regarding the likelihood of a request being unwarranted or adhering to established guidelines. By training the AutoAI model on a vast dataset of validated requests and associated outcomes, it can learn to identify patterns and make accurate determinations. The model takes into account factors such as the appropriateness of the request, optimization of the imaging strategy, risk versus benefit assessment, and age-specific considerations.

Possible features needed

- PatientID: Unique identifier for the patient.
- Symptom: Present symptom(s) experienced by the patient from the unstructured "relevant clinical information" part of the request.
- Assertion: Assertion about the presence of the symptom.
- Comorbidity: Pre-existing condition or comorbidity from the request and EMR.
- Assertion: Assertion about the condition or comorbidity.
- CounterIndication: Counter-indications from the request and EMR. Could include key conditions, medications or allergies.
- Goal: The goal or purpose of the examination from the unstructured "relevant clinical information" part of the request.
- Demographic: Patient's demographic information from the structured request.
- OrderedProcedure: Procedure ordered by the physician from the structured request.
- PastDiagnosticProcedures: History of past diagnostic procedures and "days ago" from the EMR or Radiology Information System.
- PastTherapeuticProcedures: History of past therapeutic procedures and "days ago" from the EMR.
- OrderingPhysician: Physician who ordered the procedure from the structured request.
- Age: Patient's age in years caculated field
- Gender: Patient's gender from the structured request.
- BMI: Patient's Body Mass Index from the EMR.
- Request_outcome: If the request was completed or cancelled


## Generating Response Explanations using LLMs
Once the determination is made, the AI system employs LLMs, specifically Generative models, to craft comprehensive response explanations. These explanations are tailored to the referrer, providing clear justifications for the decision made by the AI system. The generated responses aim to facilitate understanding and collaboration between the radiology department and referrers, ensuring transparency and mutual cooperation in optimizing patient care.

```
{
  "Response": "Justified",
  "Explanation": "The response is justified. The referral clearly indicates the need for a chest CT due to the patient's persistent shortness of breath and history of a lung nodule. The WatsonX.AI model assigned a high confidence score of 0.92, indicating a strong prediction that a chest CT is necessary for proper evaluation and follow-up.",
  "watsonx_ai_confidence": "0.92"
}
```



## Conclusion
The implementation of an AI project for radiology request assessment offers substantial benefits to both radiologists and referrers. By automating the vetting process and leveraging advanced technologies such as LLMs and AutoAI, the project aims to improve the efficiency and quality of referrals while reducing the burden on radiology departments. This comprehensive solution enhances the decision-making process, ensures appropriate utilization of resources, and ultimately improves patient outcomes.
